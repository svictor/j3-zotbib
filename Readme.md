This Joomla 3.x plugin displays bibliographies from libraries made publicly available on zotero.org. To insert a bibliography in your content write something like `{zotbib author="Wittgenstein" itemType="book"}`. This will be replaced by a formatted list of all the books authored by Wittgenstein which are in your Zotero library.

# Features:

*   Numerous  filters are available to match specific sets of references.
*   References can be sorted in various ways.
*   Abstracts can be displayed with a toggle button.
*   Can display iframes from the "extra" field of a reference.
*   Formatting style can be chosen in the backend.
*   Other styles can be added by uploading csl files.
*   Can work in conjunction with the Community Builder component to automatically display the bibliography of each member on his/her profile.
*   Local cache of the zotero library: only queries the local database on display.
*   Cache is periodically refreshed from zotero.org in the background.
*   Tested to work with relatively large libraries (3000+ references).

The plugin can be seen in action on the [profile pages of my research team](https://lesc-cnrs.fr/membres/userslist/35/0?Itemid=359): tabs named "Textes", "Communications", "Dans les médias" or "Multimedia".

# Usage

1. Zip the files together or choose the "download zip" option.
2. Install the package to your Joomla 3.x website.
3. Enable the plugin and set the ID of the zotero.org group/user library you want to use. The library must be publicly available at zotero.org. 

To insert a bibliography in your content write something like `{zotbib author="Wittgenstein" itemType="bookSection"}`.

## Filtering the references

The following filters are available:

*   `author` (works only on last name)
*   `editor` (ibid.)
*   `itemType` ("book", "journalArticle", "bookSection", etc.)
*   `date` (year really)
*   `tag`
*   `title`

The search is case-insensitive and ignores accents (Evans-Pritchard = èvänS-pRītcaŗD).

If several filters are specified, only references matching all of them will be displayed (implicit AND).

Any filter can be negated by prepending its value with `-` (minus sign). Example: `{zotbib tag="foo" tag="-bar"}` will display all references with tag "foo" and no tag "bar".

## Integration with Community Builder

In conjuction with `author` and `editor` filters, the plugin understands an additional value placeholder `"cbid"`. If used in a Community Builder "Custom HTML " field, `"cbid"` will be replaced by the profile owner’s lastname. You can then write e.g `{zotbib author="cbid" itemType="journalArticle"}` to display the journal articles of the profile owner. Make sure you set "Prepare content using CMS Content plugins" in the field’s parameters tab for this to work

## Sorting options

Sorting order can be specified with `sort="one-of-the-above-filters"`. Several `sort` instructions can be given in which case they will be used sequentially (`sort="author" sort="date"` will sort by author then by date). Sort order is ascending by default. Descending order is achieved by prepending the value with `-` (minus sign).

Sort order can be ommited altogether in which case it defaults to `sort="-date"` (dates, newest first).

## Local and distant databases

For quick operation, the plugin stores a local version of the library into the Joomla database. This local version is periodically synchronized with the one at zotero.org. How often sync takes place is set in the backend. Refresh can also be triggered manually from there.

Downloading large libraries (more than 500 items) can take several minutes.

*   When triggered automatically (periodical check), the refresh function tries to run uninterrupted for a time which can be specified in the backend (550 seconds by default). Many shared servers limit execution time to 5 minutes (600 seconds). If the function is shut down by the server, background refresh may not work as expected.
*   When triggered manually from the backend, the refresh function runs in small chunks of 5 seconds. This is slower but less prone to server shutdowns. The plugin’s admin page needs to remain open for this to work.

## Debugging

Debug informations are printed to the file "zotbib_log.php" in the logs folder.
