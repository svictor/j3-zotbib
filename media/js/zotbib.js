/**
 * @file
 * @brief    Plugin to display public Zotero bibliographies in Joomla
 * @author   Victor A. Stoichita
 * @version  0.1
 * @remarks  Copyright (C) 2018-20125 Victor A. Stoichita
 * @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
 * @see      http://svictor.net
 */

// refresh db function
function zotrefresh($, fragment=false, url="index.php?option=com_ajax&plugin=Zotbib&method=Zotbib&group=content&format=json") {
    $('#zotrefresh').button('loading');
    // get site base url. Needed for correct activation on CB user profiles
    var base = window.location.href.match(/^http(s)?:\/\/.*?\//);
    url = base[0] + url + '&fragment=' + fragment;
    $.get( url,
	   function ( result ) {
	       if ( result['data'].length == 0 ) { // this happens when plugin is disabled
		   $('#zotfile_refresh').attr('class', 'alert alert-warning');
		   $('#zotfile_refresh').html('Please enable plugin before refreshing!');
		   $('#zotrefresh').button('reset');
		   return;
	       }
	       // console.log(result['data'][0].response);
	       $('#zotfile_lastdate').html( result['data'][0].lastdate );
	       $('#zotfile_local').html( result["data"][0].local );
	       $('#zotfile_queries').html( result["data"][0].zotqueries );
	       $('#zotfile_dist').html( result['data'][0].dist );
	       switch (result['data'][0].response) {
	       case 'ok':
		   $('#zotfile_refresh').text( Joomla.JText._('PLG_ZOTBIB_DB_REFRESH_OK') );
		   $('#zotrefresh').button('reset');
		   $('#zotfile_refresh').attr('class', 'alert alert-success');
		   $('#zotrefresh').button('reset');
		   break;

	       case 'continue':
		   $('#zotfile_refresh').text( Joomla.JText._('PLG_ZOTBIB_DB_CONTINUE') );
		   $('#zotfile_refresh').attr('class', 'alert alert-info');
		   zotrefresh($, true);
		   break;

	       case 'ongoing':		       
		   $('#zotfile_refresh').text( Joomla.JText._('PLG_ZOTBIB_DB_ALREADY_REFRESH') );
		   $('#zotfile_refresh').attr('class', 'alert alert-warning');
		   break;

	       case 'empty':
	       case 'http_error':
		   error = new Array();
		   error[0] = result['data'][0].query;
		   error[1] = Joomla.JText._('PLG_ZOTBIB_DB_ERROR');
		   Joomla.renderMessages({'error': error}); // Render to Joomla standard message system
		   // Render below table, just in case message placeholder is absent or…
		   $('#zotfile_refresh').text( error[1] );
		   $('#zotfile_refresh').append($( '<p>' ), error[0] , $(' </p> '));
		   $('#zotfile_refresh').attr('class', 'alert alert-danger');

		   // Display error text in table rows
		   $('.zotinfo .query_result').each(function (i, cell) {
		       $(cell).text( Joomla.JText._('PLG_ZOTBIB_DB_QUERY_ERROR') );
		   });

		   $('#zotrefresh').button('reset');
		   break;

	       default: 
		   $('#zotfile_refresh').html( "Unknown error" );
	       }
	   })
	.fail(function ()  {
	    console.log ("[Zotbib] failed ajax callback");
	    $('#zotrefresh').button('reset');
	})
	.always(function() {
	}); 
}

jQuery(document).ready(function($){    
    
    // Simple block toggler for abstracts display
    $('.zotbib-abstract-link').on('click', function() {
	$parent_box = $(this).closest('.zotbib-abstract-box');
	$parent_box.siblings().find('.zotbib-abstract-text').hide();
	$parent_box.find('.zotbib-abstract-text').toggle();
    });
    

    
    $("#zotrefresh").click(function(){
	$('#zotfile_refresh').html(Joomla.JText._('PLG_ZOTBIB_DB_REFRESHING_MSG'));
	zotrefresh($, true);
    })
});


