CREATE TABLE IF NOT EXISTS `#__zotbib` (
  `zotkey` varchar(8) NOT NULL,
  `version` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `itemtype` tinytext NOT NULL,
  `tag` tinytext,
  `author` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `csljson` text NOT NULL,
  UNIQUE KEY `zotkey` (`zotkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
