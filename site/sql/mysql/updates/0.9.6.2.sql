# Allow nulls on date and title
ALTER TABLE `#__zotbib`
CHANGE `title` `title` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `editor`;

ALTER TABLE `#__zotbib`
CHANGE `date` `date` int(11) NULL AFTER `version`;
