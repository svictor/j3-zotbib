<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');



class JFormFieldZotfileInfo extends JFormField
{
    protected $type = 'ZotfileInfo';
    
    public function getInput() {
        $lastchkDate = $local = $placeholder = JTEXT::_('PLG_ZOTBIB_DB_UNKNOWN');
        
        // Get plugin params
        $plugin = JPluginHelper::getPlugin('content', 'zotbib');
        if ( $plugin ) {
            $pluginParams = new JRegistry($plugin->params);
            $local = $pluginParams->get('local', $placeholder);
            $lastchkTime = $pluginParams->get('lastchk');
            if ( !empty($lastchkTime) ) {
                $lastchkDate = date('r', $lastchkTime);
            }
        }
        
        $html = '<table class="table" id="zotFileInfo">';
        $html .= '<tr><td>' . JTEXT::_('PLG_ZOTBIB_DB_LAST_CHK') . '</td>'
              . '<td id="zotfile_lastdate" class="zotinfo local_results">' . $lastchkDate . '</td></tr>';
        $html .= '<tr><td>' . JTEXT::_('PLG_ZOTBIB_DB_LOCAL') . '</td>'
              . '<td id="zotfile_local" class="zotinfo local_results">' . $local . '</td></tr>';
        $html .= '<tr><td>' . JTEXT::_('PLG_ZOTBIB_DB_DIST')
              . '<td id="zotfile_dist" class="zotinfo query_results">' . $placeholder . '</td></tr>';
        $html .= '<tr><td>' . JTEXT::_('PLG_ZOTBIB_DB_ZOTQUERIES') . '</td>'
              . '<td id="zotfile_queries" class="zotinfo query_results">' . $placeholder . '</td></tr>';
        $html .= '</table>';
        return $html;

    }


    public function getLabel() {
        return JTEXT::_('PLG_ZOTBIB_DB_LBL');
    }

}
?>
