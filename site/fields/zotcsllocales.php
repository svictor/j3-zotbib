<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');


jimport('joomla.form.formfield');

class JFormFieldZotcslLocales extends JFormFieldList
{
    protected $type = 'ZotcslLocales';

    public function getOptions() {
        $path = JPATH_ROOT.'/media/plg_zotbib/locales/*.xml';
        foreach(glob($path) as $file){
            $name = basename($file, '.xml');
            $name = str_replace('locales-', '', $name);
            $locales[$name] = $name;
        }
        return $locales;
    }
}
?>
