<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');


class JFormFieldZotcslStyles extends JFormFieldList
{
    protected $type = 'ZotcslStyles';

    public function getOptions() {
        $path = JPATH_ROOT.'/media/plg_zotbib/styles/*.csl';
        foreach(glob($path) as $file){
            $name = basename($file, '.csl');
            $styles[$name] = $name;
        }

        return $styles;
    }

}
?>
