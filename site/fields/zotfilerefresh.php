<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldZotfileRefresh extends JFormField
{
    protected $type = 'ZotfileRefresh';

    public function getInput() {       
        return '<div id="zotfile_refresh" ></div>';
    }

    public function getLabel() {
        // Add strings for use in js file 
        JText::script('PLG_ZOTBIB_DB_REFRESHING_MSG');
        JText::script('PLG_ZOTBIB_DB_ALREADY_LASTVER');
        JText::script('PLG_ZOTBIB_DB_BAD_REQUEST');
        JText::script('PLG_ZOTBIB_DB_REFRESH_OK');
        JText::script('PLG_ZOTBIB_DB_ALREADY_REFRESH');
        JText::script('PLG_ZOTBIB_DB_ERROR');
        JText::script('PLG_ZOTBIB_DB_QUERY_ERROR');
        JText::script('PLG_ZOTBIB_DB_CONTINUE');
        
       
        
        $html = '<button id="zotrefresh" type="button" class="btn btn-primary" data-loading-text="'
              . JTEXT::_('PLG_ZOTBIB_DB_REFRESHING') . '">'
              . JTEXT::_('PLG_ZOTBIB_DB_REFRESH_BUTTON')
              . '</button>';

        // Include js
        JHtml::_('bootstrap.framework');
        $document = JFactory::getDocument();
        $document->addScript('/media/plg_zotbib/js/zotbib.js');
        
        // trigger refresh on page reload
        // $js = 'jQuery(document).ready(function($) { zotrefresh($, true); });';
        // $document->addScriptDeclaration($js);


        return $html;
    }    
}
?>
