; Asikart Extension
; Copyright (C) 2012 Asikart. All rights reserved.
; License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
; Note : All ini files need to be saved as UTF-8 - No BOM


PLG_ZOTBIB_DESCRIPTION="<p>This plugin displays bibliographies from libraries made publicly available on zotero.org. To insert a bibliography in your content write something like <code>{zotbib author=\"Wittgenstein\" itemType=\"bookSection\"}</code>.</p>
<h3>Filtering the references</h3>	
    <p>Any Zotero field could be used as a filter. The following have been tested: 
<ul>
	<li><code>author</code> (works only on last name)</li>
	<li><code>editor</code> (ibid.)</li>
	<li><code>itemType</code> (\"book\", \"journalArticle\", \"bookSection\", etc.)</li>
	<li><code>date</code> (year really)</li>
	<li><code>tag</code></li>
</ul>
<p>The search is case-insensitive and ignores accents (Amy de la Bretèque = àmy Dé lä brȩteqûe).</p>

<p>If several filters are specified, only references matching all of them will be displayed (implicit AND).</p>
<p> Any filter can be negated by prepending its value with <code>-</code> (minus sign). Example:
<code>{zotbib tag="foo" tag="-bar"}</code> will display all references with tag "foo" but no tag "bar".</p>

<h3>Integration with Community Builder</h3>

	<p>In conjuction with <code>author</code> and <code>editor</code> filters, the plugin understands an additional value placeholder <code>\"cbid\"</code>. If used in a Community Builder "Custom HTML" field, <code>\"cbid\"</code> will be replaced by the profile owner’s lastname. You can then write e.g <code>{zotbib author=\"cbid\" itemType=\"journalArticle\"}</code> to display the journal articles of the profile owner. Make sure you set "Prepare content using CMS Content plugins" in the field’s parameters tab for this to work </p>

<h3>Sorting options</h3>
<p>Sorting order can be specified with <code>sort</code> and one of the following keywords: 
<ul>
	<li><code>date</code> (in descending order, author is 2nd criterion)</li>
	<li><code>date+</code> (in ascending order, idem.)</li>
	<li><code>author</code> (date is 2nd criterion)</li>
	<li><code>authordate+</code> (idem. but with date+)</li>
	<li><code>title</code> (author and date are additional criterions if ever needed). <code>title</code> is also used as a 3rd fallback criterion after the other two.</li>
</ul>
<p>The default sort order is <code>sort=\"date\"</code>.</p>

<h3>Library cache</h3>
<p>The plugin operates on a local cache of the library. The cache is stored in a file called \"cache\" in the plugin’s folder. The plugin checks roughly every hour for a refresh (roughly because the trigger depends on the plugin being ran in frontend and/or its settings being accessed in backend). You can also refresh the cache manually from the plugin’s admin page. Downloading large libraries (more than 1000 items) can take several minutes. Once triggered the refresh runs in the background (no need to stay on the page). </p>

<h3>Debugging</h3>
<p>Debug informations are printed to the file \"zotbib_log.php\" in the logs folder.</p> 
"

PLG_ZOTBIB_STYLE_DESC="Style de citation utilisé pour formatter les références. L’identifiant du style est est la dernière partie de l’url lorsqu’on survole son nom. Ex: cahiers-d-ethnomusicologie"
PLG_ZOTBIB_STYLE_LABEL="Style de citation parmi ceux du <a href="https://www.zotero.org/styles">Zotero style repository</a>"

PLG_ZOTBIB_ISGROUP_LABEL="Type de compte"
PLG_ZOTBIB_ISGROUP_DESC="La bibliothèque zotero appartient-elle à un utilisateur ou à un groupe"
PLG_ZOTBIB_USER="Utilisateur"
PLG_ZOTBIB_GRP="Groupe"

PLG_ZOTBIB_NO="Non"
PLG_ZOTBIB_YES="Oui"


PLG_ZOTBIB_SHOW_ABSTRACT_LABEL="Afficher les résumés"
PLG_ZOTBIB_SHOW_ABSTRACT_DESC="Affiche un bouton pour afficher/masquer les résumés. Voir les fichiers css et js pour customiser l’affichage."

PLG_ZOTBIB_SHOW_IFRAME_LABEL="Afficher les iframes"
PLG_ZOTBIB_SHOW_IFRAME_DESC="Cherche les tags <iframe> dans le champ \"extra\". Ils seront affichés après la référence/résumé. Utile pour les références à des films/émissions de radio."


PLG_ZOTBIB_ZOTID_LABEL="Identifiant de l’utilisateur ou du groupe sur zotero.org"
PLG_ZOTBIB_ZOTID_DESC="L’identifiant d’un groupe figure dans l’url de la bibliothèque: https://www.zotero.org/groups/ID/etc. Pour un utilisateur l’identifiant est affiché dans l’onglet Feeds/API de vos paramètres (https://www.zotero.org/settings/keys)."

PLG_ZOTBIB_CB_USE_MNAMES_LABEL="Utiliser le champ Middlename dans les profils CB ?"
PLG_ZOTBIB_CB_USE_MNAMES_DESC="Dans les profils Community Builder, lorsque le remplacement automatique du nom de l’auteur/éditeur est activé (e.g author=\"cbid\") utiliser ou non le champ middlename du profil. Si oui sa valeur sera ajoutée après la valeur du champ Firstname."  

PLG_ZOTBIB_EXECTIME_LABEL="Durée max d’un rafraîchissement de la base (s)"
PLG_ZOTBIB_EXECTIME_DESC="Temps d’exécution maximal d’une requête de rafraîchissement de la base de données. Sa valeur doit être inférieure à la directive max_execution_time de votre serveur. Sans effet pour les rafraîchissements depuis le backend."

PLG_ZOTBIB_REFRESHTIME_LABEL="Intervalle entre deux rafraîchissements de la base (m)"
PLG_ZOTBIB_REFRESHTIME_DESC="Intervalle en minutes entre deux vérifications de la base sur zotero.org"

PLG_ZOTBIB_DB_QUERYING="Interrogeons zotero…"
PLG_ZOTBIB_DB_UNKNOWN="Inconnu"
PLG_ZOTBIB_DB_QUERY_ERROR="Requête erronnée!"
PLG_ZOTBIB_DB_LBL="Références"
PLG_ZOTBIB_DB_LAST_CHK="Dernière vérification"
PLG_ZOTBIB_DB_LOCAL="Dans la base locale"
PLG_ZOTBIB_DB_DIST="Sur zotero.org"
PLG_ZOTBIB_DB_ZOTQUERIES="À traiter"
PLG_ZOTBIB_DB_REFRESH_BUTTON="Rafraîchir"
PLG_ZOTBIB_DB_REFRESHING_MSG="Rafraîchissement de la base… Ceci pourrait prendre plusieurs minutes."
PLG_ZOTBIB_DB_ERROR="Erreur! Voici la requête envoyée. Essayez de l’exécuter dans le navigateur et/ou activez le déboggage zotbib pour plus d’infos."
PLG_ZOTBIB_DB_REFRESH_OK="La base est à jour."
PLG_ZOTBIB_DB_ALREADY_REFRESH="La base est déjà en cours de rafraîchissement."
PLG_ZOTBIB_DB_CONTINUE="Rafraîchissement en cours…"


PLG_ZOTBIB_DEBUG_LABEL="Niveau de débuggage"
PLG_ZOTBIB_DEBUG_DESC="Zotbib peut signaler ses opérations dans le répertoire de logs, fichier zotbib_log.php. Attention à l’accroissement potentiellement rapide de la taille de ce fichier, surtout si l’option TOUT est activée.." 

PLG_ZOTBIB_DEBUG_NONE="Aucun log"
PLG_ZOTBIB_DEBUG_INFO="Info (simple signalement des rafraichissements)"
PLG_ZOTBIB_DEBUG_ALL="Tout (dump complet des requêtes sql)"