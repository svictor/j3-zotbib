<?php
/**
 * @file
 * @brief    Plugin to display public Zotero bibliographies in Joomla
 * @author   Victor A. Stoichita
 * @version  0.1
 * @remarks  Copyright (C) 2018-2025 Victor A. Stoichita
 * @remarks  Licensed under GNU/GPLv3, see http://www.gnu.org/licenses/gpl-3.0.html
 * @see      http://svictor.net
 */

// no direct access
defined('_JEXEC') or die;
include __DIR__ . "/citeproc/autoload.php";
use Seboettg\CiteProc\StyleSheet;
use Seboettg\CiteProc\CiteProc;

class plgContentZotbib extends JPlugin
{
    
    // Load the language file on instantiation.
    protected $autoloadLanguage = true;

    function __construct(& $subject, $config)
    {
		parent::__construct( $subject, $config );

        $this->db = JFactory::getDbo();
        $this->zottable = $this->db->quoteName('#__zotbib');
        
        JForm::addFieldPath(__DIR__.'/fields');
        $this->document = JFactory::getDocument();

        switch ( $this->params->get('debug') ) {
        case 1: 
            $debugLevel = JLOG::INFO;
            break;
        case 2:
            $debugLevel = JLOG::ALL;
            break;
        default:
            $debugLevel = ~JLOG::ALL;
        }

        JLog::addLogger(
            array(
                // Sets file name
                'text_file' => 'zotbib_log.php',
                // Sets the format of each line
                'text_entry_format' => '{DATETIME} {PRIORITY} {MESSAGE}'
            ),
            // Sets all messages to be sent to the file
            // ( ( $this->params->get('debug', 0) == 1 ) ? JLog::ALL : ~JLog::ALL ),
            $debugLevel,
            // The log category which should be recorded in this file
            array('zotbib')
        );
    }
    
    /**
     * Gets an array of formatted references from the global library. 
     * Acts as a dispatcher called from preg_replace_callback
     * 
     * @param    array    Array of matches from the plugin call
     */
    private function getFormattedReferences($call)
    {
        $html = '';
        $db = $this->db;
        
        $legalkeys = array('date', 'itemtype', 'tag', 'author', 'editor', 'title', 'sort');
        $equalcheck = array('date', 'itemtype'); // check with WHERE `x` = 'y' instead of LIKE
        $conditions = ''; // for WHERE
        $sort = ''; // for ORDER
        
        // Find requested filters
        $filterregex = '#(\w+?)="(.*?)"#';
        $matches = preg_match_all($filterregex, $call[0], $filters);

        if ($matches == 0) return false;
        $keys = $filters[1];
        $values = $filters[2];

        // Combine them in array.
        for($i = 0; $i < count($keys); $i++) {
            $key = strtolower($keys[$i]); 
            $value = strtolower($values[$i]);
            // $value = iconv('UTF-8', 'ASCII//TRANSLIT', $value);

            if ( ! in_array($key, $legalkeys) ) continue;

            // "sort" is actually not a filter
            if ( $key == 'sort' ) {
                if ( substr($value, 0, 1) == '-' ) {
                    $order = 'DESC';
                    $value = ( substr($value, 1) );
                } else {
                    $order = 'ASC';
                }
                                
                if ( in_array($value, array_slice($legalkeys, 0, -1))) {
                    if (!empty($sort)) $sort .= ', ';
                    $sort .= $db->quoteName(strtolower($value)) . " $order";
                }
                continue; 
            }

            // If in Community Builder and value is "cbid", display profile owner’s references
            if ( $key == 'author' || $key == 'editor' ) {
                if ( $value == 'cbid' && ( defined('CBLIB') )) {
                    // get name/editor value from Community Builder
                    global $_CB_framework;
                    $uid = $_CB_framework->displayedUser();
                    $lastname = CBuser::getInstance($uid)->getUserData()->lastname;
                    $firstname = CBuser::getInstance($uid)->getUserData()->firstname;
                    $value = $lastname . ',' . $firstname;

                    if ( $this->params->get('cbUseMnames', 0) == 1 ) {
                        $middlename = CBuser::getInstance($uid)->getUserData()->middlename;
                        if ( $middlename ) $value .= ' ' . $middlename;
                    }

                    $value = strtolower($value) ;
                    // No wildcard at end. We don’t want e.g. "Jean-Luc Lambert" for "Jean Lambert".
                    // $value .= '%';
                    
                } else {
                    # remove space after comma and any trailing space,
                    $value = preg_replace('#(, *)(.*?)( *$)#', ',$2%', $value, 1, $count);
                    # if no comma, add one
                    if ( $count == 0 ) $value .= ',%';
                }
            }

            if (! empty($conditions)) {
                $conditions .= ' AND ';
            }
            
            if ( substr($value, 0, 1) == "-" ) {
                $conditions .= '( ' ;
                $value = (substr($value, 1));
                if ( in_array($key, $equalcheck) ) {
                    $conditions .= $db->quoteName($key) . ' != ' . $db->quote($value);
                } else {
                    $conditions .= $db->quoteName($key) . ' NOT LIKE ' . $db->quote( '%|' . $value . '|%');
                }
                $conditions .= ' OR ' . $db->quoteName($key) . ' IS NULL )';
            } else {
                if ( in_array($key, $equalcheck) ) {
                    $conditions .= $db->quoteName($key) . ' = ' . $db->quote($value);
                } else {
                    $conditions .= $db->quoteName($key) . ' LIKE ' . $db->quote( '%|' . $value . '|%');
                }                       
            }
        }

        if ( empty($sort) ) {
            $sort = $db->quoteName('date') . ' DESC';
        }
        
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName('csljson'))
            ->from($this->zottable)
            ->where($conditions)
            ->order($sort);

        $db->setQuery($query);

        // debug
        // print_r($db->replacePrefix( (string) $db->getQuery())); 
        
        $refs = $db->loadColumn();
        
        if ($refs) {
            $html = $this->renderBibliography($refs);
        }
        return $html;
    }

    private function renderBibliography($refs)
    {
        $html = '';
        $show_abstract = $this->params->get('show_abstract', true);
        $show_iframe = $this->params->get('show_iframe', true);
        $style = $this->params->get('style', 'chicago-author-date');
        
        $locale = $this->params->get('locale', 'en-US');
        $stylesheet = StyleSheet::loadStyleSheet($style);
        
        $citeProc = new CiteProc($stylesheet, $locale);
        
        foreach ($refs as $ref) {
            $abstract = '';
            $iframes = '';
            $ref = str_replace(array('\u201c','\u201d'), '\"', $ref); // replace “ and ” to work around buggy citeproc
            $ref = json_decode($ref);
        
            $html .= '<div class="zotbib-ref">' ;
          
            if ( $show_abstract && !empty($ref->abstract) ) {
                $abstract .= '<div class="zotbib-abstract-box">'
                          . '<div class="zotbib-abstract-link">'.JTEXT::_('PLG_ZOTBIB_ABSTRACT').'</div>'
                          .'<blockquote class="zotbib-abstract-text">'.$ref->abstract.'</div>'
                          . '</blockquote>';
                unset($ref->abstract); // avoid duplicates if csl also asks for it
            }

            if ( $show_iframe && !empty($ref->note) ) {
                if ( preg_match('#<iframe .*?</iframe>#', $ref->note, $matches) ) {
                    foreach ($matches as $match) {
                        $iframes .= '<div class="zotbib-iframe">' . $match . '</div>';
                    }
                    unset($ref->note);
                }
            }

            $html .= $citeProc->render(array($ref), "bibliography")
                  . $abstract
                  . $iframes
                  . '</div>'; // close zotbib-ref
            
            // echo '<pre>'; print_r($cslarray); echo '</pre>';
            // Make html links. Space at begining to not remove iframe srcs.
            $linkrx = '# (http(?:s)?://.+?(?=(?:(?:\.)?(?:\s|<|$))))#m';
            $html = preg_replace($linkrx, ' <a href="\1" target=_blank>\1</a>', $html);
            
        }
        return $html;
    }


    private function refresh($exectime=null)
    {
        $message = array();
        $db = $this->db;
        $zottable = $this->zottable;
        $basequery = 'https://api.zotero.org';
        $basequery .= ( $this->params->get('isgroup', True) ? '/groups' : '/users' );
        $basequery .=  '/' . $this->params->get('zotid', '913489'); // default to LESC id
        
        $initquery = $basequery . '/items/top?format=versions';
        // $initquery .= '&limit=100&start=0'; // for debug only
        // echo $initquery;
        JLog::add("Attempting initial query at $initquery", JLog::DEBUG, 'zotbib');
        
        // Lock to avoid concurrent refresh
        $lockfile = JFactory::getApplication()->getCfg('tmp_path') . 'zotbib_lock';        
        if ( $exectime == null ){
            $exectime = $this->params->get('exectime', 550); //number of secs to run for.
        }
        
        if ( file_exists($lockfile) && (time()-filemtime($lockfile)) < $exectime ) {
            JLog::add("Ongoing refresh. Cancelling new request", JLog::DEBUG, 'zotbib');
            $message['response'] = 'ongoing';
            return $message;
        } else {
            touch($lockfile);
            JLog::add("Attempting to run for $exectime seconds", JLog::DEBUG, 'zotbib');
        }

        // Query server for keys=>versions
        // Watch out these two headers in response array
        $respCodeIndex = 0; // index of http response code
        $respTotalResultsIndex = 4; // index of Total Results count
        
        $response = file_get_contents($initquery);
        if ($http_response_header[$respCodeIndex] != 'HTTP/1.1 200 OK' ) {
          $debug = print_r($http_response_header, true);
          $message['response'] = 'http_error';
          $message['query'] = $initquery;
          $message['error'] = $debug;
          unlink($lockfile);
          return $message;
        } else if ($http_response_header[$respTotalResultsIndex] == 'Total-Results: 0') {
            $debug = print_r($http_response_header, true);
            $message['response'] = 'empty';
            $message['query'] = $initquery;
            $message['error'] = $debug;
            unlink($lockfile);
            return $message;
        } else {  
            $zotversions = json_decode($response, true);
        }
        
        // Get keys=>versions from local db
        $myversq = $db->getQuery(true); // my versions query
        $myversq
            ->select($db->quoteName(array('zotkey','version')))
            ->from($zottable);
        $db->setQuery($myversq);
        $myversions = $db->loadAssocList('zotkey', 'version');
        
        // Delete items not present in distant library
        $todelete = array_diff_key($myversions, $zotversions);
        if ( !empty($todelete) ) {
            $darray = array();
            $dkeys = array_keys($todelete);

            foreach ( $dkeys as $key ) {
                $darray[] = $db->quote($key);
            }
            $dstring = implode(',', $darray); 
            $dquery = $db->getQuery(true);
            $conditions = array($db->quoteName('zotkey') . " IN ( $dstring ) ",);
            $dquery->delete($zottable)->where($conditions);
            $db->setQuery($dquery);
            $result = $db->execute();
            if ($result) {
              $message['deleted'] = $dstring;
            }
        }

        // Build array of queries to get from zotero server
        $toget = array_diff_assoc($zotversions, $myversions);
        // echo 'Toget<pre>'; print_r($toget); echo '</pre>';
        $zotqueries = array();
        foreach ($toget as $key=>$version) {
            $zotqueries[] = array(
                'zotkey' => $key,
                'version' => $version,
                'zotquery' => $basequery . '/items/' . $key . '?format=json&include=data,csljson'
            );
        }
        
        $message['zotqueries'] = count($zotqueries);
        JLog::add("Need to update " . $message['zotqueries'] . " items", JLog::INFO, 'zotbib');
        
        // Get each item to update/insert  
        foreach ($zotqueries as $zotquery) {
            $response = file_get_contents($zotquery['zotquery']);
            if ( $http_response_header[0] != 'HTTP/1.1 200 OK' ) {
                $debug = print_r($http_response_header, true);
                $message['response'] = 'error';
                $message['error'] = print_r($http_response_header, true);
                return $message;
            }
            
            $message['zotqueries']--;
            
            $response = json_decode($response);
            $data = $response->data;
            $cslobj = $response->csljson;

            // prepare variables for db insertion.
            // Column order:
            // 'zotkey', 'version', 'date', 'itemtype', 'tag', 'author', 'editor', 'title', 'csljson'
            
            $zotkey = $db->quote($data->key);
            JLog::add("Updating item $zotkey", JLog::INFO, 'zotbib');

            // echo "item $zotkey"; print_r( $response ); echo '</pre>';
            
            $version = $data->version;

            if ( !empty($cslobj->issued->{'date-parts'}[0][0])) {
                $year = $cslobj->issued->{'date-parts'}[0][0];
            } else {
                $year = 'NULL';
            }
            
            $itemtype = $db->quote(strtolower($data->itemType));

            // Combine tags
            if ( !empty($data->tags) ){
                $tags = '|';
                foreach ($data->tags as $tag) {
                    $tags .= strtolower($tag->tag) . '|';
                }
                // $tags = iconv('UTF-8', 'ASCII//TRANSLIT', $tags);
                $tags = $db->quote($tags);
            } else {
                $tags = 'NULL';
            }
            
            // Combine authors names
            if ( !empty($cslobj->author) ) {
                $authors = '|';
                foreach ($cslobj->author as $author) {
                    $authors .= strtolower($author->family) . ',';
                    if ( !empty($author->given) ) {
                        $authors .= strtolower($author->given);
                    }
                    $authors .= '|';
                }
                // $authors = iconv('UTF-8', 'ASCII//TRANSLIT', $authors);
                $authors = $db->quote($authors);
            } else {
                $authors = 'NULL';
            }
            
            // Combine editors names
            if ( !empty($cslobj->editor) ) {
                $editors = '|';
                foreach ($cslobj->editor as $editor) {
                    $editors .= strtolower($editor->family) . ',';
                    if ( !empty($editor->given) ) {
                        $editors .= strtolower($editor->given);
                    }
                    $editors .= '|';
                }
                // $editors = iconv('UTF-8', 'ASCII//TRANSLIT', $editors);
                $editors = $db->quote($editors);
            } else {
                $editors = 'NULL';
            }

            $title = strtolower($data->title);
            // $title = iconv('UTF-8', 'ASCII//TRANSLIT', $title);                                
            $title = $db->quote($title);
            
            $csljson = $db->quote(json_encode($cslobj));
            
            // Update or insert values into db
            if (array_key_exists($data->key, $myversions)){
                // updating
                $uquery = $db->getQuery(true);
                $fields = array( 
                    $db->quoteName('version') . ' = ' . $version,
                    $db->quoteName('date') . ' = ' . $year,
                    $db->quoteName('itemtype') . ' = ' . $itemtype,
                    $db->quoteName('tag') . ' = ' . $tags,
                    $db->quoteName('author') . ' = ' . $authors,
                    $db->quoteName('editor') . ' = ' . $editors,
                    $db->quoteName('title') . ' = ' . $title,
                    $db->quoteName('csljson') . ' = ' . $csljson,
                );
                $conditions = array(
                    $db->quoteName('zotkey') . ' = ' . $zotkey,
                );
                //execute query
                $uquery->update($zottable)->set($fields)->where($conditions);
                $db->setQuery($uquery);
                $result = $db->execute();
                if ($result) {
                    $debug = print_r($db->replacePrefix( (string) $db->getQuery()), true );//debug
                    JLog::add("Executed sql $debug.", JLog::DEBUG, 'zotbib');
                } else {
                    JLog::add("Problem executing sql $uquery.", JLog::DEBUG, 'zotbib');
                }
            } else {
                // Inserting. Store values in array and execute global query after loop.
                $insvalues[] = implode(',', array(
                    $zotkey,
                    $version,
                    $year,
                    $itemtype,
                    $tags, 
                    $authors,
                    $editors,
                    $title,
                    $csljson,
                ));
            }
            if ( (time()-filemtime($lockfile)) > $exectime ) {
                JLog::add("Time out of $exectime seconds reached.", JLog::DEBUG, 'zotbib');
                break;
            }
        } // end foreach $zotqueries

        // Insert new rows
        if ( isset($insvalues) ) {
            $columns = array('zotkey', 'version', 'date', 'itemtype', 'tag', 'author', 'editor', 'title', 'csljson');
            $iquery = $db->getQuery(true);
            $iquery
                ->insert($zottable)
                ->columns($db->quoteName($columns))
                ->values($insvalues);
            $db->setQuery($iquery);
            $result = $db->execute();
            if ($result) {
                $debug = print_r($db->replacePrefix( (string) $db->getQuery()), true );//debug
                JLog::add("Executed sql $debug.", JLog::DEBUG, 'zotbib');
            }
        }

        // Release lock
        unlink($lockfile);

        $cquery = $db->getQuery(true);
        $cquery->select('COUNT(*)')->from($zottable);
        
        // Reset the query using our newly populated query object.
        $db->setQuery($cquery);
        $count = $db->loadResult();

        if ( $message['zotqueries'] > 0 ) {
            $message['response'] = 'continue';
        } else {
            $message['response'] = 'ok';
        }
        $message['local'] = $count;
        $message['dist'] = count($zotversions);
        $message['lastchk'] = time(); // for storage
        $message['lastdate'] = date('r'); // for display
        
        return $message;
    }


    /**
     * Refresh the cache if needed
     * Call the function with ajax to not block the display
     * onAjaxZotbib is Joomla standard name for the call
     */
    public function onAjaxZotbib()
    {
        $frag = false;
        $frag = JFactory::getApplication()->input->get('fragment');

        // 'fragment' is given on backend refresh.
        // if given shorten exectime to update the fields 
        $exectime = ( $frag == 'true' ? 5 : false );

        // run the refresh operation
        $message = $this->refresh($exectime);

        // Write result to debug log
        $debug = print_r($message, true);
        JLog::add("Ajax query result: $debug", JLog::DEBUG, 'zotbib');

        // act according to result
        if ( $message['response'] == 'continue' ) {                
            // Write continue state
            $this->params->set('continue', true);
        } else {
            $this->params->set('continue', false);
        }

        if ( isset($message['lastchk']) ) {
            $this->params->set('lastchk', $message['lastchk']);
        }

        if ( isset($message['local']) ) {
            $this->params->set('local', $message['local']);
        }

        if ( isset($message['dist']) ) {
            $this->params->set('dist', $message['dist']);
        }

        // Write continue state. 
        $table = new JTableExtension(JFactory::getDbo());
        $table->load(array('element' => 'zotbib'));
        $table->set('params', $this->params->toString());
        $table->store();

        return $message;
    }
    
    /** 
     * Insert jQuery to trigger background refresh on document ready
     */ 
    public function insertAjaxCall()
    {        
        // trigger refresh
        $document = JFactory::getDocument();
        $js = 'jQuery(document).ready(function($) { zotrefresh($); });';
        $document->addScriptDeclaration($js);
    }

    /**
     * This method is called by the view
     *
     * @param    string    The context of the content being passed to the plugin.
     * @param    object    The content object.  
     * @param    object    The content params
     * @param    int       The 'page' number
     * @since    1.6
     */
    public function onContentPrepare($context, &$article, &$params, $page = 0)
    {
        // performance Determine whether bot should process further
        if (strpos($article->text, 'zotbib') === false ) return true;
        if ($context == 'com_finder.indexer') return true;
        // setlocale(LC_CTYPE, 'fr_FR.utf8'); // needed for iconv in parseCall()
        
        $callregex = '#{zotbib\s.*?}#s';
        // all work on the text is done here by calling getFormattedReferences
        $article->text = preg_replace_callback($callregex, array($this, 'getFormattedReferences'), $article->text, -1);

        // add css
		$this->document->addStyleSheet('/media/plg_zotbib/css/zotbib.css');
        //load bootstrap + JQuery in no-conflict mode
        JHtml::_('bootstrap.framework');
        // add js
        $this->document->addScript('/media/plg_zotbib/js/zotbib.js');

        // check if refresh needed
        static $refreshing = false;

        if ( $this->params->get('continue', false) == true && $refreshing == false ) {
            $this->insertAjaxCall();
            JLog::add("Continuing interrupted refresh", JLog::INFO, 'zotbib');
            $refreshing = true;
        }
       
        $refreshtime = $this->params->get('refreshtime', 60);
        $lastchk = $this->params->get('lastchk', 0);        
        $lapse = (time() - $lastchk) / 60;
        if ( $lapse > $refreshtime && $refreshing == false ) {
            $this->insertAjaxCall();
            JLog::add("Time for refresh. $lapse minutes since last check", JLog::INFO, 'zotbib');
            $refreshing = true;
        }
    }    
}
?>
